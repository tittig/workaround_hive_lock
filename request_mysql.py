"""
@author: Gimenez Thibault
@description: request mysql 
"""

import MySQLdb
import sys
import logging
from Singleton_logger import *



class Mysql: 
    conn = None
    
    def __init__(self, host, user, passwd, db):
        self.paramMysql = {
        'host'   : str(host),
        'user'   : str(user),
        'passwd' : str(passwd),
        'db'     : str(db)
        }
    
    def __enter__(self):
        try :
            self.conn = MySQLdb.connect(**self.paramMysql)
        except MySQLdb.Error:
            e = sys.exc_info()[1]
            logger.error("ERROR: mysql connection "+str(e))
            sys.exit(1)
            
    def __close__(self):
        try :
            self.conn.close()
        except MySQLdb.Error:
            e = sys.exc_info()[1]
            logger.error("ERROR: mysql close connection "+str(e))
            sys.exit(1)        
            
    def __getconnection__(self):
        if self.conn.open == 1 :
            return self.conn
        else :
            self.__enter__()
            return self.conn
    
#get data       
    def get_data(self, sql):
        try:
            # On  créé une conexion MySQL
            #conn = MySQLdb.connect(**self.paramMysql)
            conn = self.__getconnection__()
            # On créé un curseur MySQL
            cur = conn.cursor(MySQLdb.cursors.DictCursor)
            # On exécute la requête SQL
            cur.execute(sql)
            # On récupère toutes les lignes du résultat de la requête
            rows = cur.fetchall()
            

            
        except MySQLdb.Error:
            # En cas d'anomalie
            e = sys.exc_info()[1]
            MyLogger.__call__().get_logger().error("ERROR: get data "+str(e))
            sys.exit(1)

        finally:
            # On ferme la connexion
            if self.conn: 
                cur.close()
                #conn.close()
                return rows
            
#delete data
    def delete_data(self, sql, records_to_delete):
        try:
            # On  créé une conexion MySQL
            #conn = MySQLdb.connect(**self.paramMysql)
            conn = self.__getconnection__()
            # On créé un curseur MySQL
            cur = conn.cursor()
            try:
                # On exécute la requête SQL
                cur.execute(sql, (records_to_delete,))
                # On commit
                conn.commit()
                MyLogger.__call__().get_logger().info(cur.rowcount + " Record Deleted successfully")

            except MySQLdb.Error:
                e = sys.exc_info()[1]
                MyLogger.__call__().get_logger().error("ERROR: delete data "+str(e))
                # En cas d'erreur on annule les modifications
                conn.rollback()

        except MySQLdb.Error:
            # En cas d'anomalie
            e = sys.exc_info()[1]
            sys.exit(1)

        finally:
            # On ferme la connexion
            if self.conn:
                cur.close()
                #conn.close()
                
#delete bulk of data
# records_to_delete = [(6,), (5,)]
    def delete_data_bulk(self, sql, records_to_delete):
        try:
            # On  créé une conexion MySQL
            #conn = MySQLdb.connect(**self.paramMysql)
            conn = self.__getconnection__()
            # On créé un curseur MySQL
            cur = conn.cursor()
            try:
                # On exécute la requête SQL
                cur.executemany(sql_Delete_query, records_to_delete)
                # On commit
                conn.commit()
                MyLogger.__call__().get_logger().info(cur.rowcount + " Record Deleted successfully")

            except MySQLdb.Error:
                e = sys.exc_info()[1]
                # En cas d'erreur on annule les modifications
                conn.rollback()
                MyLogger.__call__().get_logger().error("ERROR: delete data "+str(e))

        except MySQLdb.Error:
            # En cas d'anomalie
            e = sys.exc_info()[1]
            sys.exit(1)

        finally:
            # On ferme la connexion
            if self.conn:
                cur.close()
                #conn.close()
    
#remove duplicate value
    def remove_duplicate_rows(self, rows) :
        return [i for n, i in enumerate(list(rows)) if i not in list(rows)[n + 1:]] 

#print class
    def __repr__(self):
        return  "<mysql: param:%s>" % (self.paramMysql )
        
        
        