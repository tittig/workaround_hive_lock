# Draft about hive lock workaround

**This project** is a draft to drop old blocked lock for hive 1.2.1 when acid is enable 

## Environment :
- hdp2.6
- hive.support.concurrency true
- hive.enforce.bucketing true (Not required as of Hive 2.0)
- hive.exec.dynamic.partition.mode nonstrict
- hive.txn.manager org.apache.hadoop.hive.ql.lockmgr.DbTxnManager
- hive.compactor.initiator.on true (See table below for more details)
	
## Issue description :
- https://issues.apache.org/jira/browse/HIVE-11934
- https://issues.apache.org/jira/browse/HIVE-12634
- This issue appears when there are a change of active ressource manager and no dag has been submitted

