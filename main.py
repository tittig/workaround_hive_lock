"""
@author: Gimenez Thibault
@description: delete old lock
"""

import configparser
from request_mysql import Mysql
from Singleton_logger import *


def timeline_request(host, port, HL_AGENT_INFO):
    import requests
    import json
    import pandas as pd
    from requests_kerberos import HTTPKerberosAuth

    session = requests.Session()
    session.trust_env = False
    r = session.get("http://"+host+":"+port+"/ws/v1/timeline/TEZ_DAG_ID?secondaryFilter=callerId%3A"+HL_AGENT_INFO, auth=HTTPKerberosAuth() )
    if r.status_code == requests.codes.ok :
        j= json.loads(r.text)
        r.close
        return j
    else :
        r.raise_for_status()
        sys.exit(1)

if __name__ == '__main__':
    
    #parse conf file 
    config = configparser.ConfigParser()
    config.read("./conf/lock.conf")
    config.sections()

    # create logger
    logger = MyLogger.__call__().get_logger()
    

    #create mysql connection
    logger.info('CREATE DB CONNECTION')
    conn = Mysql(config.get('DATABASE', 'host'), config.get('DATABASE', 'user'), config.get('DATABASE', 'passwd'),config.get('DATABASE', 'db'))
    conn.__enter__()
    
    #list old lock
    logger.info('LIST of OLD LOCK')
    sql = """select HL_LOCK_EXT_ID, HL_AGENT_INFO  from hivehdpbig.HIVE_LOCKS where hl_txnid not in (select txn_id from hivehdpbig.TXNS) and (CAST(1000*UNIX_TIMESTAMP(current_timestamp(3)) as UNSIGNED INTEGER) - HL_ACQUIRED_AT)> 3600000"""
    list_old_lock =  conn.get_data(sql)
    list_old_lock = conn.remove_duplicate_rows(list_old_lock)
    logger.info('List of old lock : ' + str(list_old_lock))
    
    #list of lock to delete. Compare with timeline server info
    logger.info('LIST of LOCK TO DELETE')
    list_to_delete = []
    for el in list_old_lock :
        output=timeline_request(config.get('TIMELINE_SERVER', 'host'), config.get('TIMELINE_SERVER', 'port') ,el["HL_AGENT_INFO"])
        if not any(output['entities']):
            list_to_delete.append(el["HL_LOCK_EXT_ID"])
    logger.info('LOCKS TO DELETE '+ str(list_to_delete))
    
    #delete old lock 
    logger.info('LIST of LOCK TO DELETE')
    sql_delete = """delete from hivehdpbig.HIVE_LOCKS where HL_LOCK_EXT_ID = %s"""
    for el in list_to_delete :
        logger.info('delete from hivehdpbig.HIVE_LOCKS where HL_LOCK_EXT_ID ='+str(el))
        #conn.delete_data(sql_delete, el)

    #close connection
    conn.__close__()