"""
@author: Gimenez Thibault
@description: logger singleton 
"""

import configparser
import logging
import logging.handlers as handlers
import logging.config


class SingletonType(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonType, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
    

class MyLogger(object, metaclass=SingletonType):
    _logger = None

    def __init__(self):
        logging.config.fileConfig('./conf/logging_config.ini')
        self._logger = logging.getLogger('deleteLockLogger')
        print("Generate new instance")

    def get_logger(self):
        return self._logger